define([
    'mage/utils/wrapper',
    'Nshift_Integration/js/model/deliver_to',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/action/select-shipping-address',
    'uiRegistry'
], function (
    wrapper,
    deliverTo,
    addressConverter,
    checkoutData,
    selectShippingAddress,
    uiRegistry
) {
    'use strict';

    return function (shippingRatesValidator) {
        shippingRatesValidator.validateFields = wrapper.wrapSuper(shippingRatesValidator.validateFields, function () {
            if (deliverTo.checkDeliverTo() !== 'pickup') {
                return this._super();
            }

            // Validator pickup address for rates.
            var addressFlat = checkoutData.getPickupAddressFromData(), address;

            if (this.validateAddressData(addressFlat)) {
                addressFlat = uiRegistry.get('checkoutProvider').pickupPointForm;
                address = addressConverter.formAddressDataToQuoteAddress(addressFlat);
                selectShippingAddress(address);
            }
        });

        return shippingRatesValidator;
    };
});
