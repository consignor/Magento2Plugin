/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'ko',
    'Magento_Checkout/js/checkout-data',
    'jquery',
    'mage/url'
], function (ko, checkoutData, $, urlBuilder) {
    'use strict';

    return {
        set: function(value) {
            // this.isLoading(true);
            checkoutData.setDeliverTo(value);
            this.checkDeliverTo(value);
            // var self = this;
            // save to session
            // $.ajax({
            //     type: 'POST',
            //     url: urlBuilder.build('nshiftintegration/eshopper/ChangeDeliverTo'),
            //     data:  {
            //         deliver_to: value
            //     },
            //     dataType: 'json',
            //     success: function(response, textStatus, jqXHR) {
            //         console.log('Changed deliverTo: ' + value);
            //     },
            //     complete: function (data) {
            //         self.isLoading(false);
            //     }
            // });
        },
        checkDeliverTo: ko.observable(checkoutData.getDeliverTo()),
        getValue: function() {
            return this.checkDeliverTo();
        },
        getFromSession: function() {
            $.ajax({
                type: 'POST',
                url: urlBuilder.build('nshiftintegration/eshopper/GetDeliverTo'),
                async: false,
                dataType: 'json',
                success: function(response, textStatus, jqXHR) {
                    console.log('Got deliverTo from session: ' + response.deliver_to);
                    checkoutData.setDeliverTo(response.deliver_to);
                }
            });
        },
        isLoading: ko.observable(false)
    };
});
