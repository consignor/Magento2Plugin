define([
    'jquery',
    'underscore'
],function (
    $,
    _
) {
    'use strict';

    return function (paypalOrderReview) {
        if (typeof window.isShipadviseEnabled !== 'undefined' && window.isShipadviseEnabled === false) {
            return $.mage.orderReview;
        }
        $.widget('mage.orderReview', paypalOrderReview, {
            _submitUpdateOrder: function (url, resultId) {
                var shippingMethod = $.trim($(this.options.shippingSelector).val());
                if (shippingMethod) {
                    var attributes = this._getAttributesFromMethod(shippingMethod);
                    for (var i = 0; i < attributes.length; i++) {
                        this._addAttributeHtmlToForm(attributes[i])
                    }
                }
                return this._super(url, resultId);
            },
            _getAttributesFromMethod: function (shippingMethod) {
                if (this.options.hasOwnProperty('rates') && Array.isArray(this.options.rates)) {
                    var rate = _.findWhere(this.options.rates, { code: shippingMethod });

                    if (typeof rate === 'object' && rate.hasOwnProperty('additional')
                        && this.options.hasOwnProperty('attributeCodes')
                    ) {
                        var attributes = JSON.parse(rate.additional), parsedAttributes = [];
                        for (var i = 0; i < this.options.attributeCodes.length; i++) {
                            var attributeCode = this.options.attributeCodes[i];
                            if (attributes.hasOwnProperty(attributeCode)) {
                                parsedAttributes.push({
                                    attribute_code: attributeCode,
                                    value: attributes[attributeCode]
                                });
                            }
                        }
                        return parsedAttributes;
                    }
                }

                return [];
            },
            _addAttributeHtmlToForm: function (attribute) {
                if (typeof attribute === 'object' && attribute.hasOwnProperty('attribute_code')
                    && attribute.hasOwnProperty('value')
                ) {
                    var $form = $(this.options.shippingSubmitFormSelector),
                        $attributeHtml = $form.find('input[name="'+attribute.attribute_code+'"]');

                    if (!$attributeHtml.val()) {
                        $('<input>').attr({
                            type: 'hidden',
                            name: attribute.attribute_code,
                            value: attribute.value
                        }).appendTo($form)
                    } else {
                        $attributeHtml.val(attribute.value)
                    }
                }
            }
        });

        return $.mage.orderReview;
    };
});
