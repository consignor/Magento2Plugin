/*global define*/
define([
    'ko',
    'Magento_Ui/js/form/form',
    'Nshift_Integration/js/model/pickup',
    'Nshift_Integration/js/model/droppoint',
    'Nshift_Integration/js/model/deliver_to',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'uiRegistry',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote',
    'Nshift_Integration/js/model/map-popup-state',
    'text!Nshift_Integration/template/ui/modal/map-modal-popup.html',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
    'jquery'
], function (
    ko,
    Component,
    pickup,
    droppoint,
    deliverTo,
    checkoutData,
    customerData,
    shippingRatesValidator,
    registry,
    customer,
    quote,
    mapPopupState,
    mapPopupTpl,
    modal,
    $t,
    $
) {
    'use strict';

    var mapPopUp = null;

    return Component.extend({
        isDropPointsLoading: droppoint.isDropPointsLoading,
        errorMessage: ko.observable(false),
        isMapPopupVisible: mapPopupState.isVisible,
        hasDropPointBeenSelected: droppoint.hasDropPointBeenSelected,
        initialize: function () {
            this._super();
            this.checkDropPointOnCurrentStore();
            // Persist pickup point form values
            var fieldsetName = 'checkout.steps.shipping-step.shippingAddress.before-form.pickup-point-form-container.pickup-address-form-fields';
            registry.async('checkoutProvider')(function (checkoutProvider) {
                var pickupAddressData = checkoutData.getPickupAddressFromData();
                if (pickupAddressData) {
                    checkoutProvider.set(
                        'pickupPointForm',
                        $.extend(true, {}, checkoutProvider.get('pickupPointForm'), pickupAddressData)
                    );
                    if (pickupAddressData.email) {
                        checkoutData.setInputFieldEmailValue(pickupAddressData.email);
                    }
                }
                checkoutProvider.on('pickupPointForm', function (pickupAddrsData) {
                    checkoutData.setPickupAddressFromData(pickupAddrsData);
                    checkoutData.setInputFieldEmailValue(pickupAddrsData.email);
                    pickup.setData(pickupAddrsData);
                    $('form[data-role=email-with-possible-login] input[name=username]').val(
                        pickupAddrsData.email
                    );
                    if (!customer.isLoggedIn()) {
                        quote.guestEmail = pickupAddrsData.email;
                    }
                });
                if (checkoutData.getSelectedDropPointId()) {
                    shippingRatesValidator.initFields(fieldsetName);
                }
            });

            // Subscribe to deliverTo observable
            deliverTo.checkDeliverTo.subscribe(function (value) {
                // Save value in checkout data
                deliverTo.set(value);
                // droppoint.refreshShippingMethods();
            });

            this.isMapPopupVisible.subscribe(function (value) {
                if (value) {
                    this.getMapPopup().openModal();
                } else {
                    this.getMapPopup().closeModal();
                }
            }, this);

            droppoint.errorMessage.subscribe(function (errorMessage) {
                this.errorMessage(errorMessage);
            }, this);

            droppoint.isDropPointsLoading.subscribe(function (isDropPointsLoading) {
                if (!isDropPointsLoading && !droppoint.errorMessage()) {
                    this.isMapPopupVisible(true);
                }
            }, this);

            return this;
        },
        pickup: ko.computed(function () {
            return deliverTo.getValue() === 'pickup';
        }),
    getDropPointData: function (keys = null) {
        var droppoint_data = droppoint.getDropPoint();

        if (droppoint_data && droppoint_data.id && droppoint_data.name) {
            if (keys) {
                return this.getProp(keys, droppoint_data);
            }
            return droppoint_data;
        }
        return null;
    },
        getProp: function (prop, obj) {
            if (obj === undefined || obj === null) {
                return obj;
            }
            if (!Array.isArray(prop) || (Array.isArray(prop) && prop.length === 0)) {
                return obj;
            }

            var foundSoFar = obj[prop[0]];
            var remainingProps = prop.slice(1);
            return this.getProp(remainingProps, foundSoFar);
        },
        /**
         * Form submit handler
         *
         * This method can have any name.
         */
        changeDropPoint: function () {
            droppoint.getDropPoints(this.source.get('pickupPointForm'));
            droppoint.hasDropPointBeenSelected(false);
            checkoutData.setDropPointData(null);
            checkoutData.setSelectedDropPointId(null);
        },
        onSubmit: function () {
            // trigger form validation
            this.source.set('params.invalid', false);
            this.source.trigger('pickupPointForm.data.validate');
            // set is valid as false to overwrite previous observable
            pickup.setIsValid(false);

            // verify that form data is valid and deliverTo is pickup
            if (!this.source.get('params.invalid') && deliverTo.getValue() === 'pickup') {
                // set is valid as true
                pickup.setIsValid(true);

                // data is retrieved from data provider by value of the customScope property
                var formData = this.source.get('pickupPointForm');
                pickup.setData(formData);

                // Get Drop Points
                droppoint.getDropPoints(formData);
            }
        },
        getMapPopup: function () {
            var self = this;

            if (!mapPopUp) {
                this.mapPopUpConfig.options.buttons = [];
                this.mapPopUpConfig.options.popupTpl = mapPopupTpl;
                this.mapPopUpConfig.options.autoOpen = false;

                /** @inheritdoc */
                this.mapPopUpConfig.options.closed = function () {
                    self.isMapPopupVisible(false);
                };

                this.mapPopUpConfig.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
                this.mapPopUpConfig.options.keyEventHandlers = {
                    escapeKey: this.onClosePopUp.bind(this)
                };
                mapPopUp = modal(this.mapPopUpConfig.options, $(this.mapPopUpConfig.element));
                if (this.mapPopUpConfig.options.formFieldsId) {
                    $('#'+this.mapPopUpConfig.options.formFieldsId).appendTo('#modal-form-controls')
                }
                var searchBtn = this.mapPopUpConfig.options.modalSearchBtn;
                mapPopUp.modal.on('click', searchBtn, self.onSubmit.bind(this));
                mapPopUp.openModal();
            }

            return mapPopUp;
        },
        /**
         * Revert address and close modal.
         */
        onClosePopUp: function () {
            mapPopupState.isVisible(false);
        },
        checkDropPointOnCurrentStore: function () {
            var cartData = customerData.get('cart'),
                pickupAddressData = checkoutData.getPickupAddressFromData();
            if (pickupAddressData && typeof cartData === 'function' && typeof cartData() === 'object') {
                if (pickupAddressData.storeId !== cartData().storeId) {
                    checkoutData.setDropPointData(null);
                    checkoutData.setSelectedDropPointId(null);
                    checkoutData.setPickupAddressFromData(null);
                    deliverTo.set('address');
                }
            }
        }
    });
});
