define([], function () {
    'use strict';
    return function (BaseShippingRates) {
        return BaseShippingRates.extend({
            defaults: {
                template: 'Nshift_Integration/cart/shipping-rates'
            }
        });
    }
});
