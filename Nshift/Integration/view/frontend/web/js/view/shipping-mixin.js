define([
    'underscore',
    'ko',
    'Nshift_Integration/js/model/deliver_to',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/shipping-service'
], function (
    _,
    ko,
    deliverTo,
    checkoutData,
    shippingService
) {
    'use strict';

    function getPickupRates(rates)
    {
        var pickupRates = [], i;

        for (i = 0; i < rates.length; i++) {
            if (rates[i].extension_attributes &&
                rates[i].extension_attributes.additional
            ) {
                var attributes = JSON.parse(rates[i].extension_attributes.additional);
                if (typeof attributes === 'object' && typeof attributes.droppoint_id !== 'undefined') {
                    pickupRates.push(rates[i]);
                }
            }
        }

        return pickupRates;
    }

    function getPostalRates(rates)
    {
        return _.difference(rates, getPickupRates(rates));
    }

    return function (BaseShipping) {
        return BaseShipping.extend({
            defaults: {
                template: 'Nshift_Integration/shipping'
            },
            isPickup: function () {
                return deliverTo.getValue() === 'pickup'
            },
            // Show shipping methods on pickup only if pickup point is persisted
            showShippingMethods: function () {
                if (deliverTo.getValue() === 'pickup') {
                    var droppoint_data = checkoutData.getDropPointData();

                    if (droppoint_data && droppoint_data.id) {
                        return true;
                    } else {
                        return false
                    }
                } else {
                    return true;
                }
            },
            shippingRateGroups: ko.observableArray([]),
            rates: ko.computed(function () {
                var rates = shippingService.getShippingRates();
                if (rates() && rates().length) {
                    if (deliverTo.checkDeliverTo() === 'pickup') {
                        return getPickupRates(rates());
                    } else {
                        return getPostalRates(rates());
                    }
                }
                return rates();
            }),
            initObservable: function () {
                var self = this;

                this._super();

                this.rates.subscribe(function (rates) {
                    self.shippingRateGroups([]);

                    var placeholder = [];

                    _.each(rates, function (rate) {
                        var extension_attributes = JSON.parse(rate['extension_attributes']['additional']);
                        var carrierTitle = rate['carrier_title'];

                        /*
                        if(extension_attributes['carrier_display_name']) {
                            carrierTitle = extension_attributes['carrier_display_name'];
                        }*/

                        var carrierImage = extension_attributes['carrier'];

                        let group = {
                            'carrier': carrierTitle,
                            'image': carrierImage
                        };

                        // Debug HTML with this for knockout JS
                        //<pre data-bind="text: JSON.stringify(ko.toJS(shippingRates()), null, 2)"></pre>

                        placeholder.push(group);
                    });

                    placeholder = self.getUnique(placeholder, 'carrier');
                    self.shippingRateGroups(placeholder);
                });

                return this;
            },
            getUnique: function (arr, comparison_item) {
                // store the comparison  values in array
                const unique =  arr.map(e => {
                    return e[comparison_item];
                })

                    // store the indexes of the unique objects
                    .map((e, i, final) => final.indexOf(e) === i && i)

                    // eliminate the false indexes & return unique objects
                    .filter((e) => arr[e]).map(e => arr[e]);

                return unique;
            },
            getRatesForGroup: function (shippingRateGroupTitle) {
                return _.filter(this.rates(), function (rate) {
                    return shippingRateGroupTitle === rate['carrier_title'];
                });
            },
            validateShippingInformation: function () {
                // Validation not needed for pickup point
                if (deliverTo.getValue() === 'pickup') {
                    return true;
                }

                return this._super();
            }
        });
    }
});
