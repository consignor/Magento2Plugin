define([
    'underscore'
], function (_) {
    return function (BaseDefaultComponent) {
        if (window.isShipadviseEnabled === false) {
            return BaseDefaultComponent;
        }

        return BaseDefaultComponent.extend({
            defaults: {
                template: 'Nshift_Integration/shipping-information/address-renderer/default'
            },
            removeAttributes: ['droppoint_id', 'delivery_date', 'dispatch_date', 'carrier', 'carrier_display_name'],
            filterAttributes: function (attributes) {
                var self = this;
                if (typeof attributes === 'object') {
                    for (var i = 0; i < self.removeAttributes.length; i++) {
                        if (attributes.hasOwnProperty(self.removeAttributes[i])) {
                            delete attributes[self.removeAttributes[i]];
                        }
                    }
                }
                return _.filter(attributes, function (attribute) {
                    if (typeof attribute !== 'object') {
                        return attribute;
                    }
                    return !self.removeAttributes.includes(attribute.attribute_code);
                });
            }
        });
    }
});
