define(
    [
        'ko',
        'uiComponent',
        'Nshift_Integration/js/model/deliver_to',
        'Nshift_Integration/js/model/shipping-service',
    ],
    function (ko, Component, deliverTo, shippingService) {
        "use strict";

        return Component.extend({
            defaults: {
                template: 'Nshift_Integration/deliver_to'
            },
            checkDeliverTo: deliverTo.checkDeliverTo,
            initialize: function () {
                this._super();

                this.checkDeliverTo.subscribe(function (deliverTo) {
                    if (deliverTo === 'pickup') {
                        shippingService.recollectRatesFromPickupAddress();
                    } else {
                        shippingService.recollectRatesFromPostalAddress();
                    }
                });

                return this;
            },
            getValue: function () {
                return deliverTo.getValue();
            }
        });
    }
);
