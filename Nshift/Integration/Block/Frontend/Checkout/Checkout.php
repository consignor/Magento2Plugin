<?php
/**
 * Checkout file
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
namespace Nshift\Integration\Block\Frontend\Checkout;
/**
 * Checkout class
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
class Checkout extends \Magento\Framework\View\Element\Template
{
    protected $_helperData;
    protected $checkoutSession;
    protected $scopeConfig;
    protected $configWriter;
    protected $cacheTypeList;

    /**
     * ServiceOption __construct
     *
     * @param string $context         //The context
     * @param string $carrierModel    //The carrierModel
     * @param string $checkoutSession //The checkoutSession
     * @param string $configWriter    //The configWriter
     * @param string $cacheTypeList   //The cacheTypeList
     *
     * @return null
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Nshift\Integration\Helper\Data $helperData,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    ) {
        parent::__construct($context);
        $this->_helperData = $helperData;
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $context->getScopeConfig();
        $this->configWriter = $configWriter;
        $this->cacheTypeList = $cacheTypeList;
    }

    /**
     * Checkout isActiveShipadvise
     * function check is Active Shipadvise
     *
     * @return array $data
     */
    public function isActiveShipadvise()
    {
        return $this->_helperData->isShipAdviseEnabled();
    }

    /**
     * Checkout getGoogleMapsApiKey
     * function getGoogleMapsApiKey Shipadvise
     *
     * @return false|string
     */
    public function getGoogleMapsApiKey()
    {
        return $this->_helperData->getGoogleMapsApiKey();
    }
}
