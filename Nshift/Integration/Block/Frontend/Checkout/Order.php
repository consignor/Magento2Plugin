<?php
/**
 * Order file
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
namespace Nshift\Integration\Block\Frontend\Checkout;
/**
 * Order class
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
class Order extends \Magento\Framework\View\Element\Template
{
    /**
     * Order __construct
     *
     * @param string $context //The context
     *
     * @return null
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context
    ) {
        parent::__construct($context);
    }
}
