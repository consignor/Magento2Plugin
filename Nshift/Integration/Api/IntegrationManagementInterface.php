<?php
/**
 * A Magento 2 module named Nshift/Integration
 * Copyright (C) 2017  Nshift 2018
 * 
 * This file is part of Nshift/Integration.
 * 
 * Nshift/Integration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nshift\Integration\Api;

interface IntegrationManagementInterface
{
    /**
     * GET - setToken
     * @param string $param
     * @return string
     */
    public function setToken($param);

    /**
     * GET - getConsignorData
     * @param string $token
     * @param int $order_id
     * @return string
     */
    public function getConsignorData($token, $order_id);

    /**
     * Flushes cache in magento
     * @return string
     */
    public function flushCache();
}
