<?php
/**
 * Address
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\Plugin\Quote\Model\Quote;

use Nshift\Integration\Helper\Data;
use Nshift\Integration\Model\Quote\Address\CustomAttributeList;
use Magento\Quote\Model\Quote\Address as BaseAddress;

class Address
{
    /**
     * @var Data
     */
    protected $dataHelper;
    /**
     * @var CustomAttributeList
     */
    protected $customAttributeList;

    public function __construct(
        Data $dataHelper,
        CustomAttributeList $customAttributeList
    ) {
        $this->dataHelper = $dataHelper;
        $this->customAttributeList = $customAttributeList;
    }

    public function afterBeforeSave(BaseAddress $subject, BaseAddress $result): BaseAddress
    {
        if (!$this->dataHelper->isShipAdviseEnabled()) {
            return $result;
        }
        if ($result->getAddressType() === BaseAddress::ADDRESS_TYPE_SHIPPING) {
            $method = $result->getShippingMethod();
            if ($method) {
                $rate = $result->getShippingRateByCode($method);
                if ($rate instanceof BaseAddress\Rate
                    && $rate->getData(CustomAttributeList::DROP_POINT_ID) ===
                        $result->getData(CustomAttributeList::DROP_POINT_ID)
                ) {
                    foreach ($this->customAttributeList->getAttributeCodes() as $attributeCode) {
                        // carrier is a reserved attribute on the rate model. Use carrier_title instead.
                        if ($attributeCode === 'carrier' && $rate->getCarrierTitle()) {
                            $result->setData($attributeCode, $rate->getCarrierTitle());
                        } elseif ($value = $rate->getData($attributeCode)) {
                            $result->setData($attributeCode, $value);
                        } else {
                            $result->setData($attributeCode, null);
                        }
                    }
                }
                $isPickup = $result->getData(CustomAttributeList::DROP_POINT_ID);
                if ($isPickup) {
                    $result->setSaveInAddressBook(0);
                    $result->setSameAsBilling(0);
                }
            }
        }
        return $result;
    }
}
