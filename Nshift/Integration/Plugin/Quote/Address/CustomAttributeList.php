<?php
/**
 * CustomAttributeList
 *
 */

namespace Nshift\Integration\Plugin\Quote\Address;

use Nshift\Integration\Model\Quote\Address\CustomAttributeList as QuoteAddressCustomAttributeList;

class CustomAttributeList
{
    /**
     * @var QuoteAddressCustomAttributeList
     */
    protected $quoteAddressCustomAttribute;

    public function __construct(QuoteAddressCustomAttributeList $quoteAddressCustomAttribute)
    {
        $this->quoteAddressCustomAttribute = $quoteAddressCustomAttribute;
    }

    public function afterGetAttributes(
        \Magento\Quote\Model\Quote\Address\CustomAttributeList $subject,
        array $attributes
    ): array {
        return array_merge($attributes, $this->quoteAddressCustomAttribute->getAttributes());
    }
}
