<?php
namespace Nshift\Integration\Plugin\Carrier;

use Magento\Quote\Api\Data\ShippingMethodExtensionFactory;
//In latest version 2.3.4 ShippingMethodExtensionFactory need to update with ShippingMethodInterfaceFactory
/**
 * Class Additional
 *
 */
class Additional
{
    /**
     * @var ShippingMethodExtensionFactory
     */
    protected $extensionFactory;

    /**
     * Description constructor.
     * @param ShippingMethodExtensionFactory $extensionFactory
     */
    public function __construct(
        ShippingMethodExtensionFactory $extensionFactory
    )
    {
        $this->extensionFactory = $extensionFactory;
    }

    /**
     * @param $subject
     * @param $result
     * @param $rateModel
     * @return mixed
     */
    public function afterModelToDataObject($subject, $result, $rateModel)
    {
        $extensionAttribute = $result->getExtensionAttributes() ?
            $result->getExtensionAttributes()
            :
            $this->extensionFactory->create()
        ;
        $extensionAttribute->setDescription($rateModel->getDescription());
        $extensionAttribute->setAdditional($rateModel->getAdditional());
        $result->setExtensionAttributes($extensionAttribute);
        return $result;
    }
}