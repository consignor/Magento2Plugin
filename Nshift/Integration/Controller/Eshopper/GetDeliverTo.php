<?php
/**
 * GetAllDropPoints file
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
namespace Nshift\Integration\Controller\Eshopper;
/**
 * getDeliverTo class
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
class GetDeliverTo extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $checkoutSession;
    protected $countryFactory;
    protected $carrier;
    protected $scopeConfig;
    protected $_helperData;

    /**
     * getDeliverTo __construct
     *
     * @param string $context           //The context
     * @param string $resultJsonFactory //The resultJsonFactory
     * @param string $countryFactory    //The countryFactory
     * @param string $checkoutSession   //The checkoutSession
     * @param string $carrier           //The carrier
     * @param string $scopeConfig       //The scopeConfig
     *
     * @return null
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Nshift\Integration\Model\Carrier\Shipadvise $carrier,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Nshift\Integration\Helper\Data $helperData
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->checkoutSession = $checkoutSession;
        $this->countryFactory = $countryFactory;
        $this->carrier = $carrier;
        $this->scopeConfig = $scopeConfig;
        $this->_helperData = $helperData;
        parent::__construct($context);
    }

    /**
     * getDeliverTo execute
     *
     * @return boolean
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();

        return $result->setData(array(
            'deliver_to' => $this->checkoutSession->getData('deliver_to')
        ));
    }
}