<?php
/**
 * GetDropPoints file
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
namespace Nshift\Integration\Controller\Eshopper;
/**
 * GetDropPoints class
 *
 * @category  Nshift_Integration
 * @package   Nshift_Integration
 * @author  Nshift <integrations@nshift.com>
 * @copyright 2021 Nshift, all rights reserved
 */
class GetDropPoints extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $checkoutSession;
    protected $countryFactory;
    protected $carrier;
    protected $scopeConfig;
    protected $_helperData;

    /**
     * GetDropPoints __construct
     *
     * @param string $context           //The context
     * @param string $resultJsonFactory //The resultJsonFactory
     * @param string $countryFactory    //The countryFactory
     * @param string $checkoutSession   //The checkoutSession
     * @param string $carrier           //The carrier
     * @param string $scopeConfig       //The scopeConfig
     *
     * @return null
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Nshift\Integration\Model\Carrier\Shipadvise $carrier,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Nshift\Integration\Helper\Data $helperData
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->checkoutSession = $checkoutSession;
        $this->countryFactory = $countryFactory;
        $this->carrier = $carrier;
        $this->scopeConfig = $scopeConfig;
        $this->_helperData = $helperData;
        parent::__construct($context);
    }

    /**
     * GetDropPoints execute
     *
     * @return boolean
     */
    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            $result = $this->resultJsonFactory->create();
            $params = $this->getRequest()->getParams();

            $shipping_address = $this->checkoutSession->getQuote()->getShippingAddress();

            if (!empty($params['shipping_method_code']) && !empty($shipping_address)) {
                // Parse product
                $product = $this->getSelectedProduct($params['shipping_method_code']);

                // Attribute qa or prod token
                $token = $this->_helperData->getToken();
                $webservice_endpoint = 'https://integration.consignor.com';

                if(!empty($this->_helperData->getDebug())) {
                    $token = $this->_helperData->getQAToken();
                    $webservice_endpoint = 'http://int.qa.consignor.com';

                }

                // Build request data
                $json = array(
                    'token' => $token,
                    'ups_delivery_date_required' => $this->_helperData->getUPSDeliveryDateRequired(),
                    $product['key'] => $product['value'],
                    'services' => $product['services'],
                    'receiver' => array(
                        'attention' => '',
                        'name' => '',
                        'company' => '',
                        'street1' => $shipping_address->getStreetFull(),
                        'street2' => '',
                        'city' => $shipping_address->getCity(),
                        'region' => $shipping_address->getRegion(),
                        'postcode' => $shipping_address->getPostcode(),
                        'country_code' => $shipping_address->getCountry()
                    )
                );

                $json = json_encode($json);

                // Curl request
                $ch = curl_init($webservice_endpoint . '/magento/get-drop-points');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($json))
                );

                $response = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($response, 1);
                $response['request'] = $json;

                return $result->setData($response);

            }
        }
    }

    public function getSelectedProduct($shipping_method_code) {
        $product_type = 'product_concept_id';
        $product_id = 0;

        // Remove shipadvise text from code
        $shipping_method_code = str_replace('shipadvise_','',$shipping_method_code);

        if(!empty($shipping_method_code)) {
            $option_id = $shipping_method_code;
            $exploded_option = explode('-', $option_id);

            if(!empty($exploded_option[0])) {
                $product_info = explode('_', $exploded_option[0]);

                // Check if the string has concatenated information
                if(!empty($product_info[0])) {
                    $product_id = $product_info[0];
                }

                if(count($product_info) > 1) {
                    if($product_info[0] == 'product.csid') {
                        $product_type = 'product_csid';
                    }

                    if(!empty($product_info[1])) {
                        $product_id = $product_info[1];
                    }
                }
            }

            // Add services
            $services = array();

            if(!empty($exploded_option[1])) {
                //$services = explode(',', $exploded_option[1]);
                $services = $exploded_option[1];
            }
        }

        return array(
            'key' => $product_type,
            'value' => $product_id,
            'services' => $services
        );
    }
}
