<?php

namespace Nshift\Integration\Model\Carrier;

use Nshift\Integration\Helper\Data;
use Nshift\Integration\Model\Quote\Address\CustomAttributeList;
use Magento\Checkout\Model\Session;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;

class Shipadvise extends AbstractCarrier implements
    CarrierInterface
{
    protected $_code = 'shipadvise';
    protected $logger;
    protected $_isFixed = true;
    protected $_rateResultFactory;
    protected $_rateMethodFactory;
    protected $_helperData;
    protected $_quoteFactory;
    protected $_checkoutSession;
    protected $_regionFactory;
    public $allowed_methods = [];
    /**
     * @var CustomAttributeList
     */
    protected $customAttributeList;

    /**
     * Constructor
     *
     * @param CustomAttributeList $customAttributeList
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param Data $helperData
     * @param QuoteFactory $quoteFactory
     * @param Session $checkoutSession
     * @param RegionFactory $regionFactory
     * @param array $data
     */
    public function __construct(
        CustomAttributeList $customAttributeList,
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        Data $helperData,
        QuoteFactory $quoteFactory,
        Session $checkoutSession,
        RegionFactory $regionFactory,
        array $data = []
    ) {
        $this->logger = $logger;
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_helperData = $helperData;
        $this->_quoteFactory = $quoteFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_regionFactory = $regionFactory;
        $this->customAttributeList = $customAttributeList;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Carrier getAllowedMethods
     *
     * @return array $data
     */

    public function getAllowedMethods()
    {
        return $this->allowed_methods;
    }

    /**
     * Carrier isActive
     *
     * @return array $data
     */
    public function isActive()
    {
        return $this->getConfigData('active');
    }

    /**
     * Carrier getDebug
     *
     * @return array $data
     */
    public function getDebug()
    {
        return $this->getConfigData('debug');
    }

    /**
     * Carrier getToken
     *
     * @return array $data
     */
    public function getToken()
    {
        return $this->getConfigData('token');
    }

    /**
     * Carrier getQAToken
     *
     * @return array $data
     */
    public function getQAToken()
    {
        return $this->getConfigData('qatoken');
    }

    /**
     * Carrier getGoogleMapsApiKey
     *
     * @return array $data
     */
    public function getGoogleMapsApiKey()
    {
        return $this->getConfigData('googlemapsapikey');
    }

    /**
     * Carrier getUseDeliverTo
     *
     * @return array $data
     */
    public function getUseDeliverTo()
    {
        return $this->getConfigData('usedeliverto');
    }

    /**
     * {@inheritdoc}
     */
    public function collectRates(RateRequest $request)
    {
        try {
            if (!$this->getConfigFlag('active')) {
                return false;
            }

            // Filter by delivery type
            $quote = $this->_checkoutSession->getQuote();
            $shippingAddress = $quote->getShippingAddress();
            $droppointId = $shippingAddress->getData('droppoint_id');

            // Temporary fix for magento version 2.4.x
            $exploded_droppoint = explode("\n", $droppointId);

            if(!empty($exploded_droppoint[1])) {
                $droppointId = $exploded_droppoint[1];
            }

            $deliver_to = $droppointId ? 'pickup' : 'address';

            // Find region code
            if (!empty($request->getDestRegionId())) {
                $region_code = $this->getRegionCode($request->getDestRegionId());
            } else {
                $region_code = '';
            }

            // Attribute qa or prod token
            $token = $this->getConfigData('token');
            $webservice_endpoint = 'https://integration.consignor.com';

            if(!empty($this->getConfigData('debug'))) {
                $token = $this->getConfigData('qatoken');
                $webservice_endpoint = 'http://int.qa.consignor.com';
            }

            // Build request data
            $json = [
                'token' => $token,
                'droppoint_id' => $droppointId,
                'version' => $this->_helperData->getVersion(),
                'magento_version' => $this->_helperData->getMagentoVersion(),
                'ups_delivery_date_required' => !empty($this->getConfigData('upsdeliverydaterequired')) ? 1 : 0,
                //'total' => $request->getOrderSubtotal(),
                //'currency' => $request->getBaseCurrency(),
                // TODO - get order values
                'total' => 100,
                'currency' => 'EUR',
                'receiver' => [
                    'attention' => '',
                    'name' => 'Dummy Name',
                    'company' => '',
                    'street1' => $request->getDestStreet(),
                    'street2' => '',
                    'city' => $request->getDestCity(),
                    'region' => $region_code,
                    'postcode' => $request->getDestPostcode(),
                    'country_code' => $request->getDestCountryId()
                ],
                'products' => [],
                'deliver_to' => $deliver_to,
            ];

            $products = [];

            foreach ($request->getAllItems() as $_item) {
                if (!$_item->getParentItemId() && !$_item->getParentItem()) {
                    $products[] = [
                        'quantity' => $_item->getQty(),
                        'weight' => $_item->getWeight(),
                        'ship_separately' => $_item->isShipSeparately(),
                        'has_children' => $_item->getHasChildren(),
                        'is_virtual' => $_item->getProduct()->isVirtual(),
                    ];
                }
            }

            $json['products'] = $products;
            $json = json_encode($json);

            // Curl request
            $ch = curl_init($webservice_endpoint . '/magento/get-shipping-methods');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($json)]);

            $response = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($response, 1);

            $shippingPrice = $this->getConfigData('price');

            // Standard
            if (!empty($response['shipping_methods'])) {
                $result = $this->_rateResultFactory->create();

                foreach ($response['shipping_methods'] as $key => $shipping_method) {
                    $filter = [
                        // When deliver_to is null it means we're at the delivery-address selector
                        null => 'delivery-address',
                        'address' => 'delivery-address',
                        'pickup' => 'pickup-point'
                    ];

                    if (!empty($filter[$deliver_to]) && $shipping_method['type'] == $filter[$deliver_to]) {
                        $show = 1;
                    } else {
                        $show = 0;
                    }

                    if ($shipping_method['type'] == 'pickup-point' && empty($shipping_method['drop_points'])) {
                        $show = 0;
                    } else if (!empty($shipping_method['drop_points']) && !in_array($droppointId, $shipping_method['drop_points'])) {
                        $show = 0;
                    }

                    if (!empty($show)) {
                        $method = $this->_rateMethodFactory->create();

                        if (!empty($shipping_method['custom_fields']['carrier_display_name'])) {
                            $carrier_display_name = $shipping_method['custom_fields']['carrier_display_name'];
                        } else {
                            $carrier_display_name = $shipping_method['carrier'];
                        }

                        $method->setCarrier($this->_code);
                        $method->setCarrierTitle($carrier_display_name);
                        $method->setMethod($shipping_method['code']);
                        $method->setMethodTitle($shipping_method['name']);
                        $method->setDescription($this->getDeliveryText($shipping_method, $droppointId));
                        $method->setAdditional($this->getAdditionalFields($shipping_method, $droppointId));

                        $attributes = json_decode($method->getAdditional(), true);

                        foreach ($this->customAttributeList->getAttributeCodes() as $attributeCode) {
                            if ($attributeCode === 'carrier') { // carrier is a reserved attribute for a shipping rate
                                continue;
                            }
                            if (isset($attributes[$attributeCode])) {
                                $method->setData($attributeCode, $attributes[$attributeCode]);
                            }
                        }

                        if ($request->getFreeShipping() === true || $request->getPackageQty() == $this->getFreeBoxes()) {
                            $shipping_price = '0.00';
                        } else {
                            $shipping_price = $shipping_method['price'];
                        }

                        $method->setPrice((float)$shipping_price);
                        $method->setCost((float)$shipping_price);

                        $result->append($method);

                        $this->allowed_methods[$shipping_method['code']] = $shipping_method['name'];
                    }
                }

                return $result;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $this->logger->critical('Nshift', ['exception' => $e->getMessage()]);
        }
    }

    public function getAdditionalFields($data, $droppoint_id = null)
    {
        if (!empty($data['custom_fields']['carrier_name'])) {
            $carrier = $data['custom_fields']['carrier_name'];
        } else {
            $carrier = 'unknown';
        }

        if (!empty($data['custom_fields']['carrier_display_name'])) {
            $carrier_display_name = $data['custom_fields']['carrier_display_name'];
        } else {
            $carrier_display_name = 'unknown';
        }

        if (!empty($data['cutoff_time'])) {
            $dispatch_date = $data['cutoff_time'];
        } else {
            $dispatch_date = null;
        }

        if (!empty($data['delivery_date'])) {
            $delivery_date = $data['delivery_date'];
        } else {
            $delivery_date = null;
        }

        $additional_fields = [
            'carrier' => $carrier,
            'carrier_display_name' => $carrier_display_name,
            'dispatch_date' => $dispatch_date,
            'delivery_date' => $delivery_date
        ];

        if ($droppoint_id) {
            $additional_fields['droppoint_id'] = $droppoint_id;
        }

        return json_encode($additional_fields);
    }

    public function getDeliveryText($data, $droppoint_id)
    {
        $delivery_text = '';

        if (!empty($data['cutoff_time']) && !empty($data['delivery_date'])) {
            // Add delivery date + Cutoff time from shipadvise
            $cutoff_time = date('gA', strtotime($data['cutoff_time']));
            $delivery_date = date('d F', strtotime($data['delivery_date']));

            $delivery_text .= 'Order until <b>' . $cutoff_time . '</b> for delivery on <b>' . $delivery_date . '</b>.';
        } elseif (!empty($data['custom_fields']['delivery_earliest']) && !empty($data['custom_fields']['delivery_latest'])) {
            // Display from custom fields - earliest and latest
            $delivery_text .= 'Delivered in ' . $data['custom_fields']['delivery_earliest'] . ' - ' . $data['custom_fields']['delivery_latest'] . ' days';
        } elseif (!empty($data['custom_fields']['eta_text'])) {
            // Display eta_text if available
            $delivery_text .= $data['custom_fields']['eta_text'];
        } else {
            // Show deliverty estimate not available
            $delivery_text .= 'Delivery estimate not available.';
        }

        if (!empty($droppoint_id)) {
            $delivery_text .= '<br /><small>(* Filtered by collection point selection)</small>';
        }

        return $delivery_text;
    }

    public function getRegionCode($region_id)
    {
        $region_code = null;

        if (!empty($region_id)) {
            $region = $this->_regionFactory->create()->load($region_id);
            $region_code = $region->getCode();
        }

        return $region_code;
    }
}
