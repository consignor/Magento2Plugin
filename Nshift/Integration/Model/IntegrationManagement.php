<?php
/**
 * A Magento 2 module named Nshift/Integration
 * Copyright (C) Nshift 2018
 * 
 * This file is part of Nshift/Integration.
 * 
 * Nshift/Integration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nshift\Integration\Model;

use Nshift\Integration\Api\IntegrationManagementInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ObjectManager;

class IntegrationManagement implements IntegrationManagementInterface
{
    public function __construct(
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface  $resourceConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Nshift\Integration\Helper\Data $helperData,
        ResourceConnection $resourceConnection
    )
    {
        $this->_resourceConfig = $resourceConfig;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_helperData = $helperData;
        $this->_resource = $resourceConnection;
    }

    /**
     * {@inheritdoc}
     */
    public function setToken($param)
    {
        header("Access-Control-Allow-Origin: *");

        // Saves token depending on debug setting
        if(!empty($this->_helperData->getDebug())) {
            $this->_resourceConfig->saveConfig(
                'carriers/shipadvise/qatoken',
                $param,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        } else {
            $this->_resourceConfig->saveConfig(
                'carriers/shipadvise/token',
                $param,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        }

        // Clear cache so config token can be selected
        // This used to have a shell__exec but marketplace does not approve that
        $this->flushCache();

        return 'Success! ' . $param;
    }

    /**
     * {@inheritdoc}
     */
    public function getConsignorData($token, $order_id)
    {
        header("Access-Control-Allow-Origin: *");

        $json = array();

        try {
            if(empty($order_id)) {
                throw new \Exception('Missing order_id from request.');
            }

            if(empty($token)) {
                throw new \Exception('Missing token from request.');
            }

            // Check token to secure request
            if(!empty($this->_helperData->getDebug()) && $token == $this->_helperData->getToken()) {
                $continue = true;
            } else if($token == $this->_helperData->getQAToken()) {
                $continue = true;
            } else {
                throw new \Exception('Unauthorized request.');
            }

            // Get information from database
            $connection = $this->_resource->getConnection('core_write');

            $table = $this->_resource->getTableName('sales_order_address');
            $address = $connection->fetchAll('SELECT * FROM ' . $table . ' WHERE parent_id = ' . (int)$order_id . ' ORDER BY entity_id ASC LIMIT 1');

            if(empty($address)) {
                throw new \Exception('Nshift DATA not found.');
            }

            $json = array(
                'droppoint_id' => $address[0]['droppoint_id'],
                'carrier' => $address[0]['carrier'],
                'dispatch_date' => $address[0]['dispatch_date'],
                'delivery_date' => $address[0]['delivery_date']
            );
        } catch (\Exception $e) {
            $json['error'] = $e->getMessage();
        }

        return json_encode($json);
    }

    public function flushCache()
    {
        $this->_cacheTypeList->cleanType('config');

        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
