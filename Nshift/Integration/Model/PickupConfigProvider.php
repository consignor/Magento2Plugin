<?php
/**
 * PickupConfigProvider
 *
 * @copyright Copyright © 2020 De Facto. All rights reserved.
 * @author    developers@ede-facto.com
 */

namespace Nshift\Integration\Model;

use Nshift\Integration\Helper\Data;
use Magento\Checkout\Model\ConfigProviderInterface;

class PickupConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Data
     */
    protected $helper;

    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    public function getConfig()
    {
        return [
            'googleMapOptions' => $this->helper->getGoogleMapOptions()
        ];
    }
}
