Versions - Release notes
---

- 2.5.2 - Small bugs fixed
- 2.5.1 - Added support for 2.4.x and added magento version
- 2.5.0 - Bugfixes, extra debug tracking and small modifications to module user friendliness
- 2.4.0 - Complete rebranding from Consignor to nShift
- 2.3.0 - Fixed issue with missing token on customer install
- 2.2.0 - Added Ship Orders in Bulk interface meant to speed up shipping process
- 2.1.0 - refactored customer install procedure and linked both admin plugin and checkout plugin to the same logic
- 2.0.0 - added shipping methods and drop points in checkout using Shipment Server - Shipadvisor.
- 1.2.0 - solved issue with https on production and CORS policies
- 1.1.0 - bugfixes
- 1.0.0 - initial version